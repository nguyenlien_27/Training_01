package basic;

public class inout {
	public static void main(String[] args) {
		String b = "On completion of this form and verification of your answers, you will be sent a confirmation e-mail to the e-mail address you specified."
				+"\nUsing the confirmation e-mail, you will be able to activate your account. If you fail to activate your account within seven days, it will be purged."
				+"\nYou must specify a valid e-mail address in order to receive the account confirmation e-mail.";
		System.out.println(b);
	}
}

package basic;

public class StringDemo {
	public static void main(String[] args) {
		String a = "NguyenThiLien";
		
		System.out.println(a.toUpperCase());
		System.out.println(a.substring(6, 9));
		
		String b = "   Nguyen Thi Lien  ";
		System.out.println(b);
		System.out.println(b.trim());
	}
}

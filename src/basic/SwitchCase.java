package basic;

public class SwitchCase {
	public static void main(String[] args) {
		int a = 0;
		switch (a){
			case 0: 
				System.out.println("a0");
				break;
			case 10:
				System.out.println("a10");
				break;
			case 20:
				System.out.println("a20");
				break;
			default:
				System.out.println("default");
				break;
		}
	}
}

/* Switch chỉ dùng với các kiểu dữ liệu int, short
 * nếu không có break thì nó sẽ ko thoát ra được khỏi vòng lặp và tiếp tục chạy case tiếp
*/
package basic;

import java.util.LinkedHashMap;
import java.util.Map;

public class map2 {
	public static void main(String[] args) {
		Map<String, String> mapLanguages = new LinkedHashMap<>();
	    mapLanguages.put("CSLT", "Cơ sở lập trình");
	    mapLanguages.put("C++", "C++");
	    mapLanguages.put("C#", "C Sharp");
	    mapLanguages.put("PHP", "PHP");
	    mapLanguages.put("Java", "Java");
	         
	    // phương thức keySet()
	    // sẽ trả về 1 Set chứa key có trong Map
	    for (String key : mapLanguages.keySet()) {
	        System.out.println("Key = " + key);
	    }
	}
}

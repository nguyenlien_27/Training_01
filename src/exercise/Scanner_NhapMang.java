package exercise;

import java.util.Arrays;
import java.util.Scanner;

public class Scanner_NhapMang {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		int n;

		System.out.println("Nhap vao so phan tu cua mang: ");
		n = scanner.nextInt();
		int a[] = new int[n];

		// nhap cac phan tu cho mang
		System.out.println("Nhập các phần tử cho mảng: ");
		for (int i = 0; i < n; i++) {
			System.out.print("Nhập phần tử thứ " + i + ": ");
			a[i] = scanner.nextInt();
		}

		// Hiển thị mảng vừa nhập
		System.out.println("\nMảng da nhap: ");
		for (int i = 0; i < n; i++) {
			System.out.print(a[i] + "\t");
		}

		// Sap xep mang da nhap
		Arrays.sort(a);
		System.out.println("\nMang sau khi sap xep: ");
		for (int i = 0; i < n; i++) {
			System.out.print(a[i] + "\t");
		}
		
		
		
	}
}

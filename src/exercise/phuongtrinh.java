package exercise;

public class phuongtrinh {

	public static void main(String[] args) {
		// ax2 + bx + c = 0
		double a = -3;
		double b = 2;
		double c = 1;
		double delta = (Math.pow(b, 2)) - (4 * a * c);

		if (a == 0) {
			System.out.println("Day khong phai la phuong trinh bac hai");
		}else{
			System.out.println("Day la phuong trinh bac hai");
			if(delta < 0){
				System.out.println("Phuong trinh vo nghiem");
			}else if(delta == 0){
				System.out.println("Phuong trinh co mot nghiem duy nhat: x1 = x2 = " + (-b / (2 * a)));
			}else{
				System.out.println("Phuong trinh co hai nghiem");
				System.out.println("x1 = " + ((-b + Math.sqrt(delta)) / (2*a)));
				System.out.println("x2 = " + ((-b - Math.sqrt(delta)) / (2*a)));
			}
		}
		
	}

}

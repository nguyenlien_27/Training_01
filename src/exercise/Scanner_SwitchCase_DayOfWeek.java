package exercise;

import java.util.Scanner;

public class Scanner_SwitchCase_DayOfWeek {
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Nhap gia tri cua a: ");
		int a = scanner.nextInt();
		switch(a) {
		case 1:
			System.out.println("Chu nhat");
			break;
		case 2:
			System.out.println("Thu hai");
			break;
		case 3: 
			System.out.println("Thu ba");
			break;
		case 4: 
			System.out.println("Thu tu");
			break;
		case 5: 
			System.out.println("Thu nam");
			break;
		case 6: 
			System.out.println("Thu sau");
			break;
		case 7: 
			System.out.println("Thu bay");
			break;
		default:
			System.out.println("Ban da gan sai gia tri. Chi duoc nhap tu 1 den 7");
		}
	}
}

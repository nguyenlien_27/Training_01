package exercise;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {
	public static void main(String[] args) {
	
		// print current time = mili sencond
		long time = System.currentTimeMillis();
		System.out.println(time);
		
		// print current time = nano second
		long time1 = System.nanoTime();
		System.out.println(time1);
		
		// print current date time
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		Date date = new Date();
		String currentDateTime = dateFormat.format(date);
		System.out.println(currentDateTime);
	}
}

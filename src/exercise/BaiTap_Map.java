package exercise;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class BaiTap_Map {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		Map<Integer, String> hashMap = new HashMap<>();
		hashMap.put(1, "One");
		hashMap.put(0, "Zero");
		hashMap.put(2, "Two");
		hashMap.put(4, "Four");
		hashMap.put(21, "Twenty one");
		hashMap.put(5, "Five");
		System.out.println("Map da nhap: ");
		System.out.println(hashMap);

		System.out.print("Nhap key: ");
		int n = sc.nextInt();
		System.out.println(hashMap.get(n));
	}
}

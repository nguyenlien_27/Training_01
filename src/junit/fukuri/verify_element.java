package junit.fukuri;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class verify_element {
	private WebDriver driver;
	@Before
	public void before() {
		// Open browser
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Test
	public void verify() throws InterruptedException, IOException {
		//Sign in
		driver.get("http://192.168.1.124:9000/signin");
		Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='dropdown-menu']")).click();
		driver.findElement(By.xpath("//a[contains(.,' English')]")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("ipt_id_account")).sendKeys("anime");
		driver.findElement(By.id("ipt_id_password")).sendKeys("123456a@");
		driver.findElement(By.id("btn_login")).click();
		Thread.sleep(8000);
		
		//verify login
		String Actual = driver.findElement(By.id("spn_id_user_name")).getText();
		String Expected = "Anime admin hello mm";
		Assert.assertEquals(Expected, Actual);
		
		//take photo
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/home.png"));
		
		//verify login
		String logoHeight = driver.findElement(By.xpath(".//*[@id='global-nav']/a/img")).getAttribute("height");
		String logoWidth = driver.findElement(By.xpath(".//*[@id='global-nav']/a/img")).getAttribute("width");
		String logoSrc = driver.findElement(By.xpath(".//*[@id='global-nav']/a/img")).getAttribute("src");
		
		System.out.println(logoHeight);
		System.out.println(logoWidth);
		System.out.println(logoSrc);
		/*Assert.assertEquals("30", logoHeight);
		Assert.assertEquals("30", logoWidth);*/
	}

	@After
	public void after() {
		driver.quit();
	}
}

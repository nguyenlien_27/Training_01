package junit.fukuri;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SignIn {
	public static void main(String[] args) throws InterruptedException {
		// Open browser
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get("http://192.168.1.124:9000/signin");

		// Fill data
		driver.findElement(By.xpath(".//*[@id='ipt_id_account']")).sendKeys("anime");
		driver.findElement(By.xpath(".//*[@id='ipt_id_password']")).sendKeys("123456a@");
		driver.findElement(By.xpath(".//*[@id='btn_login']")).click();
		Thread.sleep(8000);
		driver.get("http://192.168.1.124:9000/signout");
	}
}

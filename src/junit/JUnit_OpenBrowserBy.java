package junit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;

public class JUnit_OpenBrowserBy {
	private WebDriver driver; // khai bao bien driver

	@Before // dieu kien truoc test (bat firefox)
	public void before() {
		// System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Test
	public void login() throws InterruptedException, IOException {
	/*
	 * driver.get("http://192.168.1.124:9000/signin"); //mo url
	 * driver.findElement(By.xpath(".//*[@id='ipt_id_account']")).sendKeys(
	 * "anime"); //by.xpath mo bang xpath, co the thay bang by.id con sendky la
	 * nhap gia tri
	 * driver.findElement(By.xpath(".//*[@id='ipt_id_password']")).sendKeys(
	 * "123456a@");
	 * driver.findElement(By.xpath(".//*[@id='btn_login']")).click(); //lenh
	 * click Thread.sleep(8000);//doi 8 giay File src=
	 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	 * FileUtils.copyFile(src, new File("images/ScreenShot.png")); String Actual
	 * = driver.findElement(By.id("spn_id_user_name")).getText(); //tim den
	 * xpath de verify text String Expected = "Anime admin hello";
	 * Assert.assertEquals(Expected, Actual);//dau tien la gia tri mong muon,
	 * tiep theo la gia tri thuc te tren web
	 */

		driver.get("http://192.168.1.124:9000/signin");
		driver.findElement(By.id("ipt_id_account")).sendKeys("anime");
		driver.findElement(By.id("ipt_id_password")).sendKeys("123456a@");
		driver.findElement(By.id("btn_login")).click();
		Thread.sleep(3000);
		/*String VerifyText = driver.findElement(By.id("spn_id_user_name")).getText();
		Assert.assertEquals("Anime admin hello", VerifyText); */

		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("Screenshot/home.png"));
		Thread.sleep(3000);
		
/*		WebElement dropdown = driver.findElement(By.id("dropdown-menu"));
		dropdown.click();
		WebElement Logout = driver.findElement(By.xpath("//a[contains(@onclick,'cleanLocalStorage()')]"));
		Thread.sleep(8000);
		Logout.click();*/
		// String VerifyText = driver.findElement(By.id("")).getText();
		// Assert.assertEquals("お問い合わせ", VerifyText);

	}

	@After
	public void after() throws Exception {
		driver.quit(); // sau khi test xong thi thoat trinh duyet
	}
}

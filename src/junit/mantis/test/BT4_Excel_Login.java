package junit.mantis.test;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.ExcelCommon_POI;
import junit.mantis.common.Utils;
import junit.mantis.pageObject.LoginObject;

public class BT4_Excel_Login {
	//declare variable
	public String ExcelName = "testData.xlsx";
    public String SheetName_Login = "logIn";
    public String SheetName_Master = "Master";
 
	@Before
	public void before() {
		CommonFunctions.commonOpenFirefox();
	}

	@Test
	public void test() throws Exception {
		Utils.driver.get(Utils.URL);
		
		//read data from excel
		XSSFSheet ExcelDataSheet = ExcelCommon_POI.setExcelFile(ExcelName,SheetName_Login);
		String User = ExcelCommon_POI.getCellData(1, 1, ExcelDataSheet);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataSheet);
		
		Utils.driver.findElement(LoginObject.txtUsername).sendKeys(User);
		Utils.driver.findElement(LoginObject.txtPassword).sendKeys(Password);
		Utils.driver.findElement(LoginObject.btnLogin).click();
		Thread.sleep(3000);
		//Assert.assertEquals(true, Utils.driver.getPageSource().contains("Logged"));
		//String verifyLogin = Utils.driver.getPageSource();
		/*String verifyLogin = Utils.driver.findElement(By.xpath("//td[contains(.,'Logged in as: test (reporter)')]")).getText();
		String actual = "Logged in as: test (reporter)"
		System.out.println(verifyLogin);*/
		if(Utils.driver.getPageSource().contains("Logged")){
			ExcelCommon_POI.setCellData(1, 3, ExcelName, SheetName_Master ,"Pass");
		}else{
			ExcelCommon_POI.setCellData(1, 3, ExcelName,SheetName_Master ,"False");
		}
	}

	@After
	public void after() {
		Utils.driver.quit();
	}
}

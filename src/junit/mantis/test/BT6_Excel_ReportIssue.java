package junit.mantis.test;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.ExcelCommon_POI;
import junit.mantis.common.Utils;
import junit.mantis.pageObject.HomeObject;
import junit.mantis.pageObject.ReportIssueObject;
import junit.mantis.pageObject.ViewIssueObject;

public class BT6_Excel_ReportIssue {
	@Before
	public void before() throws Exception {
		CommonFunctions.commonOpenFirefox();
		CommonFunctions.commonLogIn();
	}

	@Test
	public void reportIssue() throws Exception {
		// load report issue page
		Utils.driver.findElement(HomeObject.mnuReportIssue).click();
		Thread.sleep(3000);

		// Read from excel
		XSSFSheet ExcelDataReportIssue = ExcelCommon_POI.setExcelFile(Utils.excelData, Utils.sheetReportIssue);
		String category = ExcelCommon_POI.getCellData(1, 1, ExcelDataReportIssue);
		String reproducibility = ExcelCommon_POI.getCellData(1, 2, ExcelDataReportIssue);
		String severity = ExcelCommon_POI.getCellData(1, 3, ExcelDataReportIssue);
		String priority = ExcelCommon_POI.getCellData(1, 4, ExcelDataReportIssue);
		String platform = ExcelCommon_POI.getCellData(1, 5, ExcelDataReportIssue);
		String os = ExcelCommon_POI.getCellData(1, 6, ExcelDataReportIssue);
		String osVersion = ExcelCommon_POI.getCellData(1, 7, ExcelDataReportIssue);
		String assignee = ExcelCommon_POI.getCellData(1, 8, ExcelDataReportIssue);
		String summary = ExcelCommon_POI.getCellData(1, 9, ExcelDataReportIssue);
		String description = ExcelCommon_POI.getCellData(1, 10, ExcelDataReportIssue);
		String reproduce = ExcelCommon_POI.getCellData(1, 11, ExcelDataReportIssue);
		String addInfo = ExcelCommon_POI.getCellData(1, 12, ExcelDataReportIssue);

		// input info
		Utils.driver.findElement(ReportIssueObject.slbCategory).sendKeys(category);
		Utils.driver.findElement(ReportIssueObject.slbReproducibility).sendKeys(reproducibility);
		Utils.driver.findElement(ReportIssueObject.slbSeverity).sendKeys(severity);
		Utils.driver.findElement(ReportIssueObject.slbPriority).sendKeys(priority);
		Utils.driver.findElement(ReportIssueObject.txtPlatform).sendKeys(platform);
		Utils.driver.findElement(ReportIssueObject.txtOS).sendKeys(os);
		Utils.driver.findElement(ReportIssueObject.txtOSVersion).sendKeys(osVersion);
		Utils.driver.findElement(ReportIssueObject.slbAssignee).sendKeys(assignee);
		Utils.driver.findElement(ReportIssueObject.txtSummary).sendKeys(summary);
		Utils.driver.findElement(ReportIssueObject.txtDescription).sendKeys(description);
		Utils.driver.findElement(ReportIssueObject.txtReproduce).sendKeys(reproduce);
		Utils.driver.findElement(ReportIssueObject.txtAdditionalInfo).sendKeys(addInfo);
		Utils.driver.findElement(ReportIssueObject.btnSubmitReport).click();
		Thread.sleep(3000);
		
		//Search issue
		Utils.driver.findElement(HomeObject.mnuViewIssues).click();
		Thread.sleep(3000);
		Utils.driver.findElement(ViewIssueObject.txtSearch).clear();
		Utils.driver.findElement(ViewIssueObject.txtSearch).sendKeys(summary);
		Utils.driver.findElement(ViewIssueObject.btnFilter).click();
		Thread.sleep(3000);
		
		if (Utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			ExcelCommon_POI.setCellData(5, 3, Utils.excelData, Utils.sheetMaster, "Fail");
		} else {
			ExcelCommon_POI.setCellData(5, 3, Utils.excelData, Utils.sheetMaster, "Pass");
		}
	}

	@After
	public void after(){
		Utils.driver.quit();
	}
}

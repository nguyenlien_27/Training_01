package junit.mantis.test;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.Utils;

public class BT1_LoginAndTakeScreenshot {
	@Before
	public void before() throws Exception {
		CommonFunctions.commonOpenFirefox();
		CommonFunctions.commonLogIn();
	}

	@Test
	public void loginAndTakeScreenshot() throws IOException {
		
		//String currentDateTime = CommonFunctions.getCurrentDateTime();
		
		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
		// get current date time with Date()
		Date date = new Date();
		// Now format the date
		String currentDateTime = dateFormat.format(date);
		
		//Take photo
		File src= ((TakesScreenshot)Utils.driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/"+"home_"+currentDateTime+".png"));
	}

	@After
	public void after() {
		Utils.driver.quit();
	}
}

package junit.mantis.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.Utils;
import junit.mantis.pageObject.ReportIssueObject;

public class BT2_AddNewIssueThenDelete {
	@Before
	public void before() throws Exception{
		CommonFunctions.commonOpenFirefox();
		CommonFunctions.commonLogIn();
	}
	
	@Test
	public void addNewIssue() throws InterruptedException{
		//REPORT ISSUE
		//Open report issue page and input data
		Utils.driver.findElement(By.linkText("Report Issue")).click();
		Thread.sleep(8000);
		Utils.driver.findElement(ReportIssueObject.slbCategory).sendKeys("[All Projects] Layout - CSS");
		Utils.driver.findElement(ReportIssueObject.slbReproducibility).sendKeys("always");
		Utils.driver.findElement(ReportIssueObject.slbSeverity).sendKeys("crash");
		Utils.driver.findElement(ReportIssueObject.slbPriority).sendKeys("urgent");
		Utils.driver.findElement(ReportIssueObject.txtPlatform).sendKeys("entry platform");
		Utils.driver.findElement(ReportIssueObject.txtOS).sendKeys("entry os");
		Utils.driver.findElement(ReportIssueObject.txtOSVersion).sendKeys("entry os build");
		Utils.driver.findElement(ReportIssueObject.txtSummary).sendKeys("0948251919");
		Utils.driver.findElement(ReportIssueObject.txtDescription).sendKeys("entry description");
		Utils.driver.findElement(ReportIssueObject.txtReproduce).sendKeys("entry how to reproduce");
		Utils.driver.findElement(ReportIssueObject.txtAdditionalInfo).sendKeys("entry additional information");
/*		Utils.driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[14]/td[2]/label[2]")).click();
		Utils.driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[15]/td[2]/label")).click();
		Utils.driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[16]/td[2]/input")).click();*/
		Utils.driver.findElement(By.xpath("//input[@value='Submit Report']")).click();
		Thread.sleep(3000);
		
		//View issues list
		Utils.driver.findElement(By.linkText("View Issues")).click();
		Thread.sleep(3000);		

		//Find the issue that has been added
		Utils.driver.findElement(By.xpath(".//*[@id='filters_form_closed']/table/tbody/tr/td[1]/input[1]")).sendKeys("0948251919");
		Utils.driver.findElement(By.xpath(".//*[@id='filters_form_closed']/table/tbody/tr/td[1]/input[2]")).click();
		Thread.sleep(3000);
		String VerifySummary = Utils.driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[11]")).getText();
		Assert.assertEquals("0948251919",VerifySummary);
		
		//Del the issues that has been added
		/*Utils.driver.findElement(By.name("bug_arr[]")).click();
		Utils.driver.findElement(By.name("action")).sendKeys("Delete");
		Thread.sleep(3000);
		Utils.driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[5]/td/span[1]/input[2]")).click();
		Thread.sleep(3000);
		Utils.driver.findElement(By.xpath("html/body/div[2]/form/table/tbody/tr[2]/td/input")).click();*/
		
		//Log out
		Utils.driver.findElement(By.linkText("Logout")).click();
		Thread.sleep(3000);
	}
	
	@After
	public void after(){
		
	}
}

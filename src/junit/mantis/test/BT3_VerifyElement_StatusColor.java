package junit.mantis.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.Utils;
import junit.mantis.pageObject.HomeObject;

public class BT3_VerifyElement_StatusColor {
	@Before
	public void before() throws Exception{
		CommonFunctions.commonOpenFirefox();
		CommonFunctions.commonLogIn();
	}
	@Test
	public void checkStatusColor() throws InterruptedException {
		String colorForNewStt = Utils.driver.findElement(HomeObject.colorSttNew).getAttribute("bgcolor");
		String colorForFeedbackStt = Utils.driver.findElement(HomeObject.colorSttFeedback).getAttribute("bgcolor");
		String colorForAcknowledgedStt = Utils.driver.findElement(HomeObject.colorSttAcknowledged).getAttribute("bgcolor");
		String colorForConfirmedStt = Utils.driver.findElement(HomeObject.colorSttConfirmed).getAttribute("bgcolor");
		String colorForAssignedStt = Utils.driver.findElement(HomeObject.colorSttAssigned).getAttribute("bgcolor");
		String colorResolvedStt = Utils.driver.findElement(HomeObject.colorSttResolved).getAttribute("bgcolor");
		String colorColosedStt = Utils.driver.findElement(HomeObject.colorSttClosed).getAttribute("bgcolor");

		Assert.assertEquals("#fcbdbd", colorForNewStt);
		Assert.assertEquals("#e3b7eb", colorForFeedbackStt);
		Assert.assertEquals("#ffcd85", colorForAcknowledgedStt);
		Assert.assertEquals("#fff494", colorForConfirmedStt);
		Assert.assertEquals("#c2dfff", colorForAssignedStt);
		Assert.assertEquals("#d2f5b0", colorResolvedStt);
		Assert.assertEquals("#c9ccc4", colorColosedStt);
		if (colorForNewStt.equals("#fcbdbd") && colorForFeedbackStt.equals("#e3b7eb")
				&& colorForAcknowledgedStt.equals("#ffcd85") && colorForConfirmedStt.equals("#fff494")
				&& colorForAssignedStt.equals("#c2dfff") && colorResolvedStt.equals("#d2f5b0")
				&& colorColosedStt.equals("#c9ccc4")) {
			System.out.println("Pass");
		} else {
			System.out.println("False");
		}
		
	}
	
	@After
	public void after() throws Exception {
		Utils.driver.quit();
	}
}

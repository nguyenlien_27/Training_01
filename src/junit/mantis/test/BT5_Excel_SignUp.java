package junit.mantis.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.Utils;

public class BT5_Excel_SignUp {
	@Before
	public void before() {
		CommonFunctions.commonOpenFirefox();
	}

	@Test
	public void signUpNewAcc() throws InterruptedException {
		Utils.driver.get("http://sangbui.vn/mantis/signup_page.php");
		Thread.sleep(3000);
		String a = Utils.driver.findElement(By.xpath("html/body/div[2]/form/table/tbody/tr[4]/td")).getText();
		String b = "On completion of this form and verification of your answers, you will be sent a confirmation e-mail to the e-mail address you specified."
				+"\nUsing the confirmation e-mail, you will be able to activate your account. If you fail to activate your account within seven days, it will be purged."
				+"\nYou must specify a valid e-mail address in order to receive the account confirmation e-mail.";
		
		System.out.println(a);
		System.out.println(b);
		Assert.assertEquals(b, a);
	}

	@After
	public void after() {

	}
}

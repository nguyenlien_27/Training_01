package junit.mantis.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import junit.mantis.common.CommonFunctions;
import junit.mantis.common.Utils;

public class demo {
	@Before
	public void before(){
		CommonFunctions.commonOpenFirefox();
	}

	@Test
	public void demo1() throws Exception {
		CommonFunctions.commonLogIn();
		String color1 = Utils.driver.findElement(By.xpath("//td[contains(.,'new')]")).getAttribute("bgcolor");
		System.out.println(color1);
	}

	@After
	public void after() {

	}
}

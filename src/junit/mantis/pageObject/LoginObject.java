package junit.mantis.pageObject;

import org.openqa.selenium.By;

public class LoginObject {
	//log in items
		public static By txtUsername = By.name("username");
		public static By txtPassword = By.name("password");
		public static By btnLogin = By.xpath("//input[@value='Login']");
}

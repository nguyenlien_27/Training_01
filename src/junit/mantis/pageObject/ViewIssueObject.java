package junit.mantis.pageObject;

import org.openqa.selenium.By;

public class ViewIssueObject {
	//search text box
		public static By txtSearch = By.name("search");
//		public static By txtSearch = By.xpath("//input[@class=' firepath-matching-node']");
	
	//Note text box
		public static By txtAddNote = By.name("bugnote_text");

		//content for verify search result
		public static By lblVerifySearch = By.xpath("//span[@class='floatleft firepath-matching-node']");
		
		//create permalink link
		public static By lCreatePermalink = By.linkText("Create Permalink");
		//click on the first issue to view detail
		public static By lEditTheFistIssue = By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[2]");

		//action list
		public static By slbAction = By.name("action");
		
		//check all bugs on list
		public static By chkAllBug = By.name("all_bugs");
		//check one bug on list
		public static By chkOneIssue = By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[1]/input");
		
		//Ok button
		public static By btnOK = By.xpath("//input[@value='OK']");
		//Search button (to filter)
		public static By btnFilter = By.name("filter");
		//Delete button
		public static By btnDelete = By.xpath("//input[@value='Delete Issues']");
		//Close issue button
		public static By btnCloseIssue = By.xpath("html/body/div[2]/form/table/tbody/tr[4]/td/input");
		//Resolve issue button
		public static By btnResolveIssue = By.xpath("html/body/div[2]/form/table/tbody/tr[4]/td/input");
		//Update priority button
		public static By btnUpdatePriority = By.xpath("html/body/div[2]/form/table/tbody/tr[2]/td/input");
}

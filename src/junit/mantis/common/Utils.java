package junit.mantis.common;

import org.openqa.selenium.WebDriver;

public class Utils {
	public static WebDriver driver;
	public static String firefoxDriver = "geckodriver.exe";
	public static String chromeDriver = "chromedriver.exe";
	
	public static final String URL = "http://sangbui.vn/mantis/login_page.php"; 
	public static final String username = "test";
	public static final String password = "123456";
	
	
	public static String excelData = "testData.xlsx";
	public static String sheetMaster = "Master";
	public static String sheetLogin = "logIn";
	public static String sheetReportIssue = "reportIssue";
}

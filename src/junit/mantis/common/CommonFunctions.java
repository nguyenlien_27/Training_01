package junit.mantis.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.junit.Assert;
import org.openqa.selenium.firefox.FirefoxDriver;

import junit.mantis.pageObject.LoginObject;

public class CommonFunctions {
	//login
	public static void commonLogIn() throws Exception {
		Utils.driver.get(Utils.URL);
		/*Utils.driver.findElement(LoginObject.txtUsername).sendKeys(Utils.username);
		Utils.driver.findElement(LoginObject.txtPassword).sendKeys(Utils.password);
		 */		
		XSSFSheet ExcelDataLogIn = ExcelCommon_POI.setExcelFile(Utils.excelData, Utils.sheetLogin);
		String Username = ExcelCommon_POI.getCellData(1, 1, ExcelDataLogIn);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataLogIn);

		Utils.driver.findElement(LoginObject.txtUsername).sendKeys(Username);
		Utils.driver.findElement(LoginObject.txtPassword).sendKeys(Password);
		
		Utils.driver.findElement(LoginObject.btnLogin).click();
		Thread.sleep(6000);
		Assert.assertEquals(true, Utils.driver.getPageSource().contains("Logged"));
	}
	
	//Open Firefox
	public static void commonOpenFirefox()
	{
		System.setProperty("webdriver.gecko.driver",Utils.firefoxDriver);
		Utils.driver = new FirefoxDriver();
	}
	
	//Get current datetime
	public static String getCurrentDateTime() {
		// Create object of SimpleDateFormat class and decide the format
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmm");
		// get current date time with Date()
		Date date = new Date();
		// Now format the date
		String currentDateTime = dateFormat.format(date);

		return currentDateTime;
	}
}

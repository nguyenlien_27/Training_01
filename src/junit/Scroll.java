package junit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Scroll {
	public static void main(String[] args) throws Exception {
		WebDriver driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://jqueryui.com");
		Thread.sleep(5000);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		
		//Scroll down
		//((JavascriptExecutor)driver).executeScript("scroll(0,400)");
		jse.executeScript("window.scrollBy(0,400)", "");
		//jse.executeScript("scroll(0,400)");
		Thread.sleep(4000);
		
		//Scroll up
		jse.executeScript("window.scrollBy(0,-400)", "");
		//jse.executeScript("scroll(0, -250);");
		Thread.sleep(3000);
		
		//Scrolling to Bottom of a page
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		Thread.sleep(3000);
		
		//Scrolling to element
		jse.executeScript("arguments[0].scrollIntoView(true);",driver.findElement(By.xpath("//h2[contains(.,'Dive In!')]")));
		Thread.sleep(3000);
		
		driver.quit();
		
	}
}
